﻿
namespace Interfaces
{
    public interface IChangeSetting
    {
        bool ChangeLevel { get; set; }

        bool ToggleMasterVolume { get; set; }
    }
}
